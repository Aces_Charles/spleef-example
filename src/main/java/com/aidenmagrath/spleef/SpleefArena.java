package com.aidenmagrath.spleef;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.aidenmagrath.minigameframework.arena.Arena;


public class SpleefArena extends Arena
{

	public SpleefArena(String name, String game, int minPlayers, int maxPlayers, Location lobby)
	{
		super(name, game, minPlayers, maxPlayers, lobby);
	}

	@Override
	public void addPlayer(Player player)
	{
		super.addPlayer(player);
		
		//Add minigame specific things here.
	}

	@Override
	public void startGame()
	{
		super.startGame();
		addItems();
	}

	@Override
	protected void addItems()
	{
		super.addItems();
		//Give all players in game a shovel
	}
	
	
}
