package com.aidenmagrath.spleef;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.aidenmagrath.minigameframework.MinigameFramework;
import com.aidenmagrath.minigameframework.arena.Arena;
import com.aidenmagrath.minigameframework.arena.ArenaManager;


public class Spleef extends JavaPlugin
{
	//Test Minigame
	
	public static String game = "Spleef";
	
	@Override
	public void onEnable()
	{
		MinigameFramework framework = (MinigameFramework) Bukkit.getPluginManager().getPlugin("MinigameFramework");
		
		if(framework != null)
		{
			framework.addMinigame(game);
			
			World world = Bukkit.getServer().getWorld("world");
			ArenaManager.getManager().addArena(new SpleefArena("spleefTest", game, 2, 5, new Location(world, -15.5, 70.5, 166.5)));
		} else
		{
			Bukkit.getPluginManager().disablePlugin(this);
		}
		
	}
}
